import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.util.*;
import java.lang.*;

class ServerCommand extends Thread {

    Socket socket = null;
	 Robot robot = null;
	 boolean isDone = false;

	 //Robot, digunakan sebagai perantara antara OS dengan Input pada Java
	 //Robot menggunakan mouse sebagai media perantara
    public ServerCommand(Socket socket, Robot robot) {
        this.socket = socket;
        this.robot = robot;
    }

    public void run(){
        Scanner scanner = null;
        try {
            //prepare Scanner object
            scanner = new Scanner(socket.getInputStream());
				//kiriman data dari client berupa perintah

            while(!isDone){
                int command = scanner.nextInt();
				    int input = scanner.nextInt();
					 
					 if (command == -1) robot.mousePress(input);
					 else if (command == -2) robot.mouseRelease(input);
					 else if (command == -3) robot.keyPress(input);
					 else if (command == -4) robot.keyRelease(input);
					 else if (command == -5) robot.mouseMove(input,scanner.nextInt()); //2 data dikirim, koordinat X Y
					 else System.out.println("Aneh, kemungkinan ada serangan");
            }
        } catch (Exception e) {
            System.out.println("Error pada command "+e.getMessage());
        }
    }
}

class ScreenSpyer extends Thread {

    Socket socket = null; 
    Robot robot = null; // capture screen
    Rectangle rectangle = null; //rectangle -> dimensi layar
    boolean continueLoop = true;
    
    public ScreenSpyer(Socket socket, Robot robot,Rectangle rect) {
        this.rectangle = rect;
		  this.socket = socket;
        this.robot = robot;
    }

    public void run()
	 {
        ObjectOutputStream oos = null; //Stream untuk kirim data
        try
		  {
            //Stream untuk mengirim capture screen
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(rectangle);

       		while(continueLoop){
            	//Screen Capture pake robot
	            BufferedImage image = robot.createScreenCapture(rectangle);
	            ImageIcon imageIcon = new ImageIcon(image);
             	oos.writeObject(imageIcon); //kirim object bertipe ImageIcon
             	oos.reset(); //Clear ObjectOutputStream cache
             	Thread.sleep(100);
            }
        }catch (Exception e){System.out.println("Error pada screen : "+e.getMessage());}

    }
}

public class Host {

    Socket socket = null;

    public static void main(String[] args){
			Scanner sc = new Scanner(System.in);
			System.out.print("Masukkan Port untuk me-listen : "); int port = sc.nextInt();        
			new Host().initialize(port);
    }

    public void initialize(int port){

        Robot robot = null; //Used to capture the screen
        Rectangle rectangle = null; //Used to represent screen dimensions

        try {
				ServerSocket sc = new ServerSocket(port);
				socket = sc.accept();

				System.out.println("Listening controller. . .");
            //Get default screen device
            GraphicsEnvironment gEnv=GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice gDev=gEnv.getDefaultScreenDevice();

            //Dimensi layar
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            rectangle = new Rectangle(dim);

            //Java Robot
            robot = new Robot(gDev);
            new ScreenSpyer(socket,robot,rectangle).start(); //thread untuk kirim gambar
            new ServerCommand(socket,robot).start(); //thread untuk menerima perintah dari server 

				Socket command = sc.accept();
				Algorithm A = new Algorithm(command);				
				while (true)
				{
					runCommand(A.recv(),A);
				}
        } catch (Exception e){System.out.println("Error : "+e.getMessage());}
    }

	 public void runCommand(String C, Algorithm A)
	 {
		//digunakan untuk menjalankan command pada shell
		try 
		{
			BufferedReader br = new BufferedReader(
						new InputStreamReader(Runtime.getRuntime().
						exec(C).getInputStream()
				)
			);

			while (br.ready()) {
				System.out.println(br.readLine());
			}
			
			String line;
		   while((line = br.readLine()) !=null)
			{
 		   	System.out.println(line);
				A.send(line);
			}

		}
		catch (Exception e) {
			System.out.println("Error pada command "+e.getMessage());
		}
	}
}
