/*****
Created By : Habibie Faried
Date : 17 Juli 2013
Deskripsi : File ini merupakan algoritma socket dasar yang digunakan untuk
membuat program Remote Desktop bagian CMD
******/

import java.net.*;
import java.io.*;
import java.util.*;
import java.lang.*;

class Algorithm extends Thread
{
	private Socket So;

	public Algorithm(Socket So)
	{
		this.So = So;
	}

	public void send(String S)
	{
		//Prosedur ini untuk mengirimkan data berupa string
		try
		{
			DataOutputStream out =  new DataOutputStream(So.getOutputStream());
		   out.writeUTF(S);		
		}	
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public String recv()
	{
		try
		{
			DataInputStream in = new DataInputStream(So.getInputStream());
			return in.readUTF();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return "";
	}

	public void run() //menerima data, tapi thread
	{
		//Prosedur ini untuk menerima data berupa string, mengembalikan string
		while (true)
		{		
			try
			{
				DataInputStream in = new DataInputStream(So.getInputStream());
				System.out.println(in.readUTF());
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}
}
