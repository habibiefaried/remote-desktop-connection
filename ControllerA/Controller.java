import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.beans.PropertyVetoException;
import java.lang.*;
import java.util.Scanner;

class CommandSender implements KeyListener,MouseMotionListener,MouseListener {
    private Socket cSocket = null;
    private JPanel cPanel = null;
    private PrintWriter writer = null;
    private Rectangle clientScreenDim = null;

	//Belum diimplementasikan
    public void mouseDragged(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
	 public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void keyTyped(KeyEvent e) {}
    
	 public CommandSender(Socket s, JPanel p, Rectangle r) {
        cSocket = s;
        cPanel = p;
        clientScreenDim = r;

		  //menambahkan listener kedalam panel
        cPanel.addKeyListener(this); 
        cPanel.addMouseListener(this);
        cPanel.addMouseMotionListener(this);
        try {
             //Printwriter untuk mengirimkan data lewat socket, masuk ke Scanner Penerima
            writer = new PrintWriter(cSocket.getOutputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
  	 public void mousePressed(MouseEvent e) {
    //mendeteksi adanya mouse click
        writer.println(-1);
        int button = e.getButton();
        int xButton = 16;
        if (button == 3) {
            xButton = 4;
        }
        writer.println(xButton);
        writer.flush();
    }

    public void mouseReleased(MouseEvent e) {
    //mendeteksi adanya mouse release
        writer.println(-2); //kode -2

        int button = e.getButton();
        int xButton = 16;
        if (button == 3) {
            xButton = 4;
        }
        writer.println(xButton);
        writer.flush();
    }

    public void keyPressed(KeyEvent e) {
	 //mendeteksi adanya keyboard ditekan
        writer.println(-3); //kode -3
        writer.println(e.getKeyCode());
        writer.flush();
    }

    public void keyReleased(KeyEvent e) {
    //mendeteksi adanya keyboard dilepas
        writer.println(-4);
        writer.println(e.getKeyCode());
        writer.flush();
    }

	public void mouseMoved(MouseEvent e) {
	 //mendeteksi adanya gerakan mouse
        
		  writer.println(-5); //kode -5
		  //mendeteksi koordinat mouse pada layar
		  double X = clientScreenDim.getWidth()/cPanel.getWidth();
        double Y = clientScreenDim.getHeight()/cPanel.getHeight();
        
        writer.println((int)(e.getX() * X));
        writer.println((int)(e.getY() * Y));
        writer.flush();
    }
}

class ScreenReceiver extends Thread {
    private ObjectInputStream cObjectInputStream = null;
    private JPanel cPanel = null;
    private boolean isDone = false;

    public ScreenReceiver(ObjectInputStream ois, JPanel p) {
        cObjectInputStream = ois;
        cPanel = p;
    }
    public void run()
	 {  
		/* Fungsi ini digunakan untuk menerima object dari socket
			Object tersebut merupakan ImageIcon */
	    try
		 {
	       while(!isDone)
			 {
	       	ImageIcon imageIcon = (ImageIcon) cObjectInputStream.readObject(); //typecast Object -> ImageIcon
	         Image image = imageIcon.getImage(); //getImage method
	         image = image.getScaledInstance(cPanel.getWidth(),cPanel.getHeight(),Image.SCALE_FAST);

	         Graphics graphics = cPanel.getGraphics(); //get graphics dari JPanel
	         graphics.drawImage(image, 0, 0, cPanel.getWidth(),cPanel.getHeight(),cPanel); //draw image pada JPanel
	       }
	    }
		catch (Exception e){System.out.println(e.getMessage());}
    }
}


class ClientHandler extends Thread {
    private JDesktopPane desktop = null;
    private Socket cSocket = null;
    private JInternalFrame interFrame = new JInternalFrame("Client Screen",true, true, true);
    private JPanel cPanel = new JPanel();
    
    public ClientHandler(Socket cSocket, JDesktopPane desktop) {
        this.cSocket = cSocket;
        this.desktop = desktop;
    }

    public void drawGUI(){
        interFrame.setLayout(new BorderLayout());
        interFrame.getContentPane().add(cPanel,BorderLayout.CENTER); //Internal frame ditambah JPanel
        interFrame.setSize(100,100);
        desktop.add(interFrame); //desktop pane dimasukkan InternalFrame

        try {
            interFrame.setMaximum(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }

        cPanel.setFocusable(true);
        interFrame.setVisible(true);
    }

    public void run(){
        Rectangle clientScreenDim = null; //screen dimension
        ObjectInputStream ois = null; //object input stream, untuk menerima object dari socket
        drawGUI();

        try
		  {
            //Read client screen dimension
            ois = new ObjectInputStream(cSocket.getInputStream());
            clientScreenDim =(Rectangle) ois.readObject(); //typecast rectangle
				
				new ScreenReceiver(ois,cPanel).start(); //thread untuk menerima screenshot
	         new CommandSender(cSocket,cPanel,clientScreenDim);
 		  }
		  catch (Exception e){System.out.println("Error pada Client Handler : "+e.getMessage());}
    }
}

public class Controller {
    //Main server frame
    private JFrame frame = new JFrame(); 
    private JDesktopPane desktop = new JDesktopPane(); //desktop pane

    public static void main(String args[]){
		 Scanner sc = new Scanner(System.in);
		  System.out.print("Masukkan IP Host : "); String IP = sc.nextLine();
		  System.out.print("Masukkan Port Host : "); int port = sc.nextInt();
        new Controller().initialize(IP,port);
    }

    public void initialize(String IP, int port)
	 {
        try
		  {
				Socket client = new Socket(IP, port);
            drawGUI();            
            new ClientHandler(client,desktop).start(); //client handler

				Socket commands = new Socket(IP,port); //socket khusus untuk menerima command dari terminal
				String str;		
				Algorithm A = new Algorithm(commands);	
				A.start(); //thread untuk menerima keluaran hasil remote	
				while (true)
				{
					Scanner scan = new Scanner(System.in);
					System.out.print("Command> "); str = scan.nextLine();
					A.send(str);
				}
					
        }
		  catch (Exception e) {
            System.out.println("Controller Error : "+e.getMessage());
				System.exit(0);
        }
    }

    public void drawGUI()
	 {
		   frame.add(desktop,BorderLayout.CENTER); //add desktop pane
		   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //close button
		   frame.setExtendedState(frame.getExtendedState()|JFrame.MAXIMIZED_BOTH); //maksimal frame
		   frame.setVisible(true);
    }
}
